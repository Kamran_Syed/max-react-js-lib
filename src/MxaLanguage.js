import React, { Component, PropTypes } from 'react';
import { Button, ButtonToolbar, OverlayTrigger, Popover, MenuItem } from 'react-bootstrap';

	class MxaLanguage extends React.Component {
		constructor(props) {
			super(props);
			
			this.handleClick = this.handleClick.bind(this);
			this.state = {
				data:null
			}
		}
		
		componentDidMount() {
			console.log("language did mount");
			this.loadData();
		}
		
		getLanguageButton(languageID, data){
			console.log("language getLanguageButton");
			let index =0;
			
			if(languageID == ''){
				return '';
			}
			
			for	(index = 0; index < data.length; index++) {
				
				let myField = data[index];
				if(myField["LanguageID"] != languageID){
					continue;
				}
				let languageName = "";
				let pic ="";
				
				for (var key in myField) {
				  if (myField.hasOwnProperty(key)) {
					
					if(key == "LanguageName"){
						languageName = myField[key];
					}else if(key == "LanguageIcon"){
						pic = myField["LanguageIcon"];
					}
				  }
				}
				
				let picsrc = "data:image/jpg;base64,"+pic;
				return (<div className="dropdown-toggle" style={{paddingTop:'4px'}} data-toggle="dropdown"><img src={picsrc} />&nbsp;{languageName}&nbsp;</div>);
				
			}
			
			return '';
		}
		
		createFields(data){
			console.log("language createFields");
			let fields = [];
			let index =0;
			
			for	(index = 0; index < data.length; index++) {
				
				let myField = data[index];
				let languageName = "";
				let pic ="";
				
				for (var key in myField) {
				  if (myField.hasOwnProperty(key)) {

					if(key == "LanguageName"){
						languageName = myField[key];
					}else if(key == "LanguageIcon"){
						pic = myField["LanguageIcon"];
					}
				  }
				}
				let picsrc = "data:image/jpg;base64,"+pic;
				fields.push(<MenuItem onSelect={this.handleClick} id={languageName} key={index}><img onClick={this.handleClick} src={picsrc} /> {languageName}</MenuItem>);
				
			}
			
			return fields;
		}
		
		loadData(){
			console.log("language load data");

			$.ajax({
			  method: "GET",
			  url: this.props.fillUrl
			}).done(function( data ) {
				console.log("language gotdata");
				this.setState({
				  data: data
				});
			}.bind(this)).fail(function( msg ) {
				console.log("language load data failed "+msg);
			}.bind(this));
			
		}
		
		handleClick(x){
			console.log("click");
			console.log(x.target.innerText);
			if (x.target.innerText == ''){
				console.log(x.target.parentNode.innerText);
			}
		}

		render() {
			if(this.state.data == null){
				return false;
			}
			const mnuLanguages = this.createFields(this.state.data);
			const btnLanguages = this.getLanguageButton(this.props.languageID, this.state.data);
			
			return (
					<div className="dropdown MxaLanguageButton img-rounded">
						{btnLanguages}
						<ul className="dropdown-menu open">
						{mnuLanguages}
						</ul>
					</div>
			)
		}
	}	
	
export default MxaLanguage;


					