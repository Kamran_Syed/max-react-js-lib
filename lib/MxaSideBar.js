'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MxaMenuSection = require('./MxaMenuSection');

var _MxaMenuSection2 = _interopRequireDefault(_MxaMenuSection);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaSideBar = function (_React$Component) {
	_inherits(MxaSideBar, _React$Component);

	function MxaSideBar(props) {
		_classCallCheck(this, MxaSideBar);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaSideBar).call(this, props));

		_this.state = { menuLoaded: false, menuData: null };

		_this.handleClick = _this.handleClick.bind(_this);

		return _this;
	}

	_createClass(MxaSideBar, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			this.loadMenu();
		}
	}, {
		key: 'loadMenu',
		value: function loadMenu() {
			console.log("load menu");

			$.ajax({
				method: "GET",
				url: this.props.menuUrl
			}).done(function (data) {
				console.log("menu gotdata");
				this.setState({
					menuData: data,
					menuLoaded: true
				});
			}.bind(this)).fail(function (msg) {
				console.log("load menu failed " + msg);
			}.bind(this));
		}
	}, {
		key: 'createMenu',
		value: function createMenu() {
			var items = [];
			var sections = [];
			var sectionHeader = [];
			var index = 0;
			var sectionData = [];

			for (index = 0; index < this.state.menuData.length; index++) {

				var myField = this.state.menuData[index];

				var section = myField['MenuSubArea'];
				var icon = myField['MenuItemIcon'];
				var displayText = myField['DisplayText'];

				if (sections.indexOf(section) < 0) {
					console.log('New Section=' + section);

					sections.push(section);
					if (sectionHeader['rk'] != undefined) {
						items.push(_react2.default.createElement(_MxaMenuSection2.default, { header: sectionHeader, items: sectionData, showAll: true, key: index }));
						sectionHeader = [];
						sectionData = [];

						sectionHeader['rk'] = index;
						sectionHeader['icon'] = icon;
						sectionHeader['section'] = section;
					} else {
						//first header
						sectionHeader['rk'] = index;
						sectionHeader['icon'] = icon;
						sectionHeader['section'] = section;
					}
				}

				sectionData.push(_react2.default.createElement(
					'li',
					{ key: index, onClick: this.handleClick },
					_react2.default.createElement('i', { className: icon }),
					' ',
					displayText
				));
			}

			items.push(_react2.default.createElement(_MxaMenuSection2.default, { header: sectionHeader, items: sectionData, showAll: true, key: index }));

			return items;
		}
	}, {
		key: 'handleClick',
		value: function handleClick(x) {
			var btn = x.target.outerText.toLowerCase();
			console.log(btn);
			this.props.btnClicked(btn);
		}
	}, {
		key: 'render',
		value: function render() {

			if (this.state.menuLoaded == false) {
				return false;
			}
			console.log('Sidebar render');
			var menu = this.createMenu();

			return _react2.default.createElement(
				'div',
				{ className: 'col-md-3 MxaSideBar' },
				menu
			);
		}
	}]);

	return MxaSideBar;
}(_react2.default.Component);

exports.default = MxaSideBar;