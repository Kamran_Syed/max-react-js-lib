import React, { Component, PropTypes } from 'react';

	class MxaFwButton extends React.Component {
		constructor(props) {
			super(props);
			this.state = {animation:false};
		}
		
		render() {
			var width_btn = {'minWidth':'5em'};
			
			return (
					<button type="button" className={this.props.color} style={width_btn} onClick={this.props.handleClick}>{this.props.btnName}</button>
				)
		}
	}

export default MxaFwButton;