import React, { Component, PropTypes } from 'react';

	class MxaFwStep extends React.Component {
		constructor(props) {
			super(props);
			this.state = {active:this.props.active}
		}
		
		render() {
			if(this.state.active == true){
				return (
					<li className="active_form mxa_li">
							<span className="mxa_active" style={this.props.styles.stepNumber} key="1"> {this.props.step} </span>
							<span className="mxa_active_text" style={this.props.styles.stepText} key="2">
							<i className="fa fa-check"></i> {this.props.stepText} </span>
					</li>
				)
			}else{
				return (
					<li className="not_active mxa_li">
							<span className="mxa_not_active" style={this.props.styles.stepNumber} key="1"> {this.props.step} </span>
							<span className="mxa_not_active_text" style={this.props.styles.stepText} key="2">
							<i className="fa fa-check"></i> {this.props.stepText} </span>
					</li>
				)
			}
		}
	}
	
	/* return (
					<li className="active_form mxa_li">
							<span className="mxa_active" style={this.props.styles.stepNumber}> {this.props.step} </span>
							<span className="mxa_active_text" style={this.props.styles.stepText}>
							<i className="fa fa-check"></i> {this.props.stepText} </span>
					</li>
				)
	 */

export default MxaFwStep;