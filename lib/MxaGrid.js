'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrapTable = require('react-bootstrap-table');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaGrid = function (_React$Component) {
	_inherits(MxaGrid, _React$Component);

	function MxaGrid(props) {
		_classCallCheck(this, MxaGrid);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaGrid).call(this, props));

		_this.getKeys = _this.getKeys.bind(_this);
		_this.getObjIndex = _this.getObjIndex.bind(_this);
		_this.updateData = _this.updateData.bind(_this);

		_this.state = { hdrColums: [], gotData: false, stru: [], data: [{ id: 0, city: "Dublin" }] };
		return _this;
	}

	_createClass(MxaGrid, [{
		key: 'getJsonData',
		value: function getJsonData(url) {
			$.getJSON(url, function (result) {
				//if array add myid to each object
				var tData = [];
				if (Array.isArray(result)) {
					result.forEach(function (obj, idx) {
						obj.myid = idx;
						tData.push(obj);
					});
				}

				this.setState({
					data: tData,
					gotData: true
				});
				this.props.setWait("none");
			}.bind(this));
		}
	}, {
		key: 'getJsonStru',
		value: function getJsonStru(struUrl, dataUrl) {
			$.getJSON(struUrl, function (result) {

				var getHeaderColumn = function getHeaderColumn(item, idx) {

					var myret = _react2.default.createElement(
						_reactBootstrapTable.TableHeaderColumn,
						{ dataField: item.Field, isKey: false, dataAlign: 'left', dataSort: true, key: idx },
						item.Field
					);

					return myret;
				};

				var headerColumns = result.map(getHeaderColumn);

				var myKeyColumn = _react2.default.createElement(
					_reactBootstrapTable.TableHeaderColumn,
					{ dataField: 'myid', isKey: true, hidden: true, dataAlign: 'left', dataSort: true, key: headerColumns.length + 1 },
					'myid'
				);
				headerColumns.push(myKeyColumn);

				this.setState({
					hdrColums: headerColumns,
					gotData: false
				});
				this.state.stru = result;
				this.props.gotStru(result);
				this.getJsonData(dataUrl);
			}.bind(this));
		}
	}, {
		key: 'componentDidMount',
		value: function componentDidMount() {

			this.getJsonStru(this.props.struUrl, this.props.dataUrl);
		}
	}, {
		key: 'componentWillUpdate',
		value: function componentWillUpdate(nextProps, nextState) {
			console.log("Grid will update");
			if (nextProps.struUrl != this.props.struUrl) {
				console.log("Calling get json Stru");
				this.getJsonStru(nextProps.struUrl, nextProps.dataUrl);
			} else if (nextProps.dataUrl != this.props.dataUrl) {
				console.log("Calling get json data");
				this.getJsonData(nextProps.dataUrl);
			}
		}
	}, {
		key: 'getKeys',
		value: function getKeys() {
			var keys = [];
			this.state.stru.forEach(function (obj) {

				if (obj.Key == "PRI") {
					keys.push(obj.Field);
				}
			});

			return keys;
		}
	}, {
		key: 'getObjIndex',
		value: function getObjIndex(tobj, keys) {

			for (var n = 0; n < this.state.data.length; n++) {
				var obj = this.state.data[n];

				switch (keys.length) {
					case 1:
						if (obj[keys[0]] == tobj[keys[0]]) {
							return n;
						}
						break;
					case 2:
						if (obj[keys[0]] == tobj[keys[0]] && obj[keys[1]] == tobj[keys[1]]) {
							return n;
						}
						break;
					case 3:
						if (obj[keys[0]] == tobj[keys[0]] && obj[keys[1]] == tobj[keys[1]] && obj[keys[2]] == tobj[keys[2]]) {
							return n;
						}
						break;
					case 4:
						if (obj[keys[0]] == tobj[keys[0]] && obj[keys[1]] == tobj[keys[1]] && obj[keys[2]] == tobj[keys[2]] && obj[keys[3]] == tobj[keys[3]]) {
							return n;
						}
						break;
				}
			};

			return -1;
		}
	}, {
		key: 'updateData',
		value: function updateData(tmpRow) {
			var keys = this.getKeys();
			var idx = this.getObjIndex(tmpRow, keys);

			if (idx != -1) {
				this.state.data[idx] = tmpRow;
				console.log("Row updated in grid");
			} else {
				//add new row
				console.log("Row not found in grid. Inserting");
				this.state.data.push(tmpRow);
			}
		}
	}, {
		key: 'deleteData',
		value: function deleteData(tmpRow) {
			var keys = this.getKeys();
			var idx = this.getObjIndex(tmpRow, keys);

			if (idx != -1) {
				this.state.data.splice(idx, 1);
				console.log("Row deleted in grid");
			}
		}
	}, {
		key: 'handleReload',
		value: function handleReload() {
			console.log('mxagrid reload by reload button');
			this.props.setWait("half");
			this.getJsonStru(this.props.struUrl, this.props.dataUrl);
		}
	}, {
		key: 'handleNew',
		value: function handleNew() {
			this.props.onNew();
		}
	}, {
		key: 'render',
		value: function render() {

			if (this.props.visible == true) {
				//form is visible so hide grid
				return false;
			}

			var showTbl = { 'display': 'none' };
			var tableHeaders = [_react2.default.createElement(
				_reactBootstrapTable.TableHeaderColumn,
				{ dataField: 'id', isKey: true, dataAlign: 'center', dataSort: true, key: '1' },
				'id'
			), _react2.default.createElement(
				_reactBootstrapTable.TableHeaderColumn,
				{ dataField: 'city', dataAlign: 'center', dataSort: true, key: '2' },
				'city'
			)];

			if (this.state.gotData) {
				showTbl = { 'display': 'block' };
				tableHeaders = this.state.hdrColums;

				console.log("Grid will render");
				if (Object.keys(this.props.newRow).length > 0) {
					console.log("we have new row");
					this.updateData(this.props.newRow);
				}

				if (Object.keys(this.props.delRow).length > 0) {
					console.log("we have del row");
					this.deleteData(this.props.delRow);
				}
			}

			return _react2.default.createElement(
				'div',
				{ style: showTbl },
				_react2.default.createElement(
					_reactBootstrapTable.BootstrapTable,
					{ data: this.state.data, striped: true, hover: true, condensed: true, pagination: true, deleteRow: false, search: true, exportCSV: true, options: { sizePerPageList: [10, 15], onRowClick: this.props.gotRow }, selectRow: { mode: "radio", clickToSelect: true, hideSelectColumn: true, bgColor: "#d9edf7" }, onReload: this.handleReload.bind(this), onNew: this.handleNew.bind(this) },
					tableHeaders
				)
			);
		}
	}]);

	return MxaGrid;
}(_react2.default.Component);

exports.default = MxaGrid;