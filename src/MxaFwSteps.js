import React, { Component, PropTypes } from 'react';

	class MxaFwSteps extends React.Component {
		constructor(props) {
			super(props);
			this.state = { steps:this.props.steps, stepActive:3, barStyle:{}}
			this.state.barStyle = this.setBarStyle();
		}
		
		setBarStyle(){
			let barStyle = this.props.styles.barStyle;
			barStyle.width = ((this.state.stepActive / this.state.steps.length) * 100) + '%';
			
			return barStyle;
		}
		
		handleBack(){
			
			this.props.onBack();
		}
		
		render() {
			let barWidth = ((this.state.stepActive / this.state.steps.length) * 100) + '%';
			return (
				<div >
					<div className="row">
						<div className="col-sm-1">
							<img className="img-responsive MxaBack" src="/Content/images/back.png" onClick={this.handleBack.bind(this)} />
						</div>
						<div className="col-sm-11">
							<ul className="mxa_ul">
								{this.state.steps}
							</ul>
						</div>
					</div>
					<div className="row">
						<div className="col-md-12">
							<div className="progress">
								<div className="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow={barWidth} aria-valuemin="0" aria-valuemax="100" style={this.state.barStyle}>
								</div>
							</div>
						</div>
					</div>
				</div>
			)
		}
	}
	
export default MxaFwSteps;