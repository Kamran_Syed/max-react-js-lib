import "babel-polyfill";
import React, { Component, PropTypes } from 'react';
import MxaFwFormPart from './MxaFwFormPart';
import MxaFwFormParts from './MxaFwFormParts';
import MxaFwStep from './MxaFwStep';
import MxaFwSteps from './MxaFwSteps';
import MxaFwButton from './MxaFwButton';
import MxaMessage from './MxaMessage';

	class MxaFormWizard extends React.Component {
		constructor(props) {
			super(props);
			
			this.onClickNew = this.onClickNew.bind(this);
			this.onClickUpdate = this.onClickUpdate.bind(this);
			this.onClickDelete = this.onClickDelete.bind(this);
			this.onClickMaintain = this.onClickMaintain.bind(this);
			this.handleForm = this.handleForm.bind(this);
			
			
			this.state = { stepObjects: this.fillStepObjects(1, this.getStyles()),
							stepActive:1,
							btnClicked:"none",
							btnLastClicked:"none",
							message: "none",
							messageType: "success",
							clear: false
						}
			if(this.props.newClick == true){
				this.state.btnLastClicked = this.state.btnClicked;
				this.state.btnClicked = "new";
				this.state.message = "none";
				this.state.clear = true;
				
			}
		}
		
		componentWillReceiveProps(nextProps){
			if(nextProps.newClick != this.props.newClick){
				console.log("MxaFormWizard.js will receive props ");

				this.setState({
					btnLastClicked:this.state.btnClicked,
					btnClicked:"new",
					message:"none",
					clear:true
				});
			}
		}
		
		componentWillUpdate(nextProps, nextState){

		}
		
		getStyles(){
			let styles = {};
			styles.stepText = {
				color:'#333'
			}
			
			styles.stepNumber = {
				color:'blue'
			}
			
			styles.barStyle = {
				//height:'5px',
				width:'25%'
			}
			
			return styles;
		}
		
		
		fillStepObjects(stepActive, styles){
			let stepObjects = [];
			
			this.props.steps.forEach(function(entry) {
				let isActive = false;
				
				if(entry.step <= stepActive){
					isActive = true;
				}
				
				stepObjects.push( React.createElement(MxaFwStep, {step:entry.step, stepText:entry.stepText, active:isActive, styles:styles}) );
				
			});
			
			return stepObjects;
		}
		
		handleForm(step, data){
			console.log(this.state.btnClicked + this.state.btnLastClicked);
			
			if(this.state.btnClicked == "update" && this.state.btnLastClicked != "new"){
				console.log("update click");
				this.updateApi(data);
			}else if(this.state.btnClicked == "update" && this.state.btnLastClicked == "new"){
				console.log("insert click");
				this.insertApi(data);
			}else if(this.state.btnClicked == "delete"){
				console.log("delete click");
				this.deleteApi(data);
			}
			
		}
		
		
		
		
		
		insertApi(arrData){
			this.props.setWait("half");
			this.state.message = "none";
			let tdata = {};
			console.log("tdata");
			
			Object.keys(arrData).forEach(function (key) {
			   tdata[key] = arrData[key];
			});
			
			console.log(tdata);
			$.ajax({
			  method: "POST",
			  url: this.props.dataUrl,
			  data: tdata
			}).done(function( msg ) {
				this.props.setWait("none");
				this.props.updateData(tdata);
				
				this.setState({
					messageType: "success",
					message: "Data Saved",
					clear: true
				});
			}.bind(this)).fail(function( msg ) {
				//do not clear form
				this.props.setWait("none");
				this.setState({
					messageType: "error",
					message: "Cannot Save Data",
					btnClicked: "new",
					clear: false
				});
				
			}.bind(this));
			  
		}
		
		updateApi(tdata){
			this.props.setWait("half");
			this.state.message = "none";
			$.ajax({
			  method: "PUT",
			  url: this.props.dataUrl,
			  contentType: "application/json",
			  data: tdata
			}).done(function( msg ) {
				this.props.setWait("none");
				this.props.updateData(tdata);
				
				this.setState({
					messageType: "success",
					message: "Data Saved",
					clear: true
				});
			}.bind(this)).fail(function( msg ) {
				//do not clear form
				this.props.setWait("none");
				this.setState({
					messageType: "error",
					message: "Cannot Save Data"
				});
				
				
			}.bind(this));
			  
		}
		
		deleteApi(tdata){
			this.props.setWait("half");
			this.state.message = "none";
			$.ajax({
			  method: "DELETE",
			  url: this.props.dataUrl,
			  contentType: "application/json",
			  data: tdata
			}).done(function( msg ) {
				this.props.setWait("none");
				this.props.deleteData(tdata);
				
				this.setState({
					messageType: "success",
					message: "Data Deleted",
					clear: true
				});
			}.bind(this)).fail(function( msg ) {
				//do not clear form
				this.props.setWait("none");
				this.setState({
					messageType: "error",
					message: "Cannot Delete Data"
				});
				
			}.bind(this));
			  
		}
		
		onClickNew(){
			this.state.btnLastClicked = this.state.btnClicked;
			this.state.btnClicked = "new";
			this.state.message = "none";
			this.setState({
				clear: true
			});
		}
		
		onClickUpdate(){
			this.state.btnLastClicked = this.state.btnClicked;
			this.state.btnClicked = "update";
			this.refs.mxaFwFormPart.refs.mxaFwForm.submit();
			
		}
		
		onClickDelete(){
			this.state.btnLastClicked = this.state.btnClicked;
			this.state.btnClicked = "delete";
			this.refs.mxaFwFormPart.refs.mxaFwForm.submit();
		}
		
		onClickMaintain(){
			console.log("maintain click");
			this.props.callMaintain();
		}
		
		componentWillUpdate(nextProps, nextState){
			console.log('MxaFormWizard will update');
			if(nextProps.row != this.props.row){
				console.log('MxaFormWizard state reset');
				this.state.message = "none";
				this.state.btnClicked = "none";
				this.state.btnLastClicked = "none";
			}
		}
		
		handleBack(){
			this.props.onBack();
		}
		
		
		render() {
				let myClear = false;
				
				if(this.props.visible == false){
					return false;
				}
				
				if(this.state.clear == true){
					myClear = true;
					this.state.clear = false;
					
				}
				
				return (
					<div >
						<MxaFwSteps steps={this.state.stepObjects} styles={this.getStyles()} onBack={this.handleBack.bind(this)} />
						<MxaMessage message={this.state.message} interval="3000" type={this.state.messageType} />
						<MxaFwFormPart  data={this.props.row} stru={this.props.stru} handleForm={this.handleForm}  clear={myClear} ref="mxaFwFormPart"/>
						<div className="row">
							<div className="col-md-2">
								<MxaFwButton btnName="New" handleClick={this.onClickNew} color={"btn btn-success"} ref="btnNew"/>
							</div>
							<div className="col-md-2">
								<MxaFwButton btnName="Update" handleClick={this.onClickUpdate} color={"btn btn-info"} ref="btnUpdate"/>
							</div>
							<div className="col-md-2">
								<MxaFwButton btnName="Delete" handleClick={this.onClickDelete}  color={"btn btn-danger"}  ref="btnDelete"/>
							</div>
							<div className="col-md-2">
								<MxaFwButton btnName="Maintain Text" handleClick={this.onClickMaintain}  color={"btn btn-warning"}  />
							</div>
						</div>
					</div>
				)

			
		}
	}

export default MxaFormWizard;