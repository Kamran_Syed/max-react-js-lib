'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

require('babel-polyfill');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactEasyForm = require('react-easy-form');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaFwFormPart = function (_React$Component) {
	_inherits(MxaFwFormPart, _React$Component);

	function MxaFwFormPart(props) {
		_classCallCheck(this, MxaFwFormPart);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaFwFormPart).call(this, props));

		_this.handleForm = _this.handleForm.bind(_this);

		_this.state = { step: 0, active: false, completed: false };
		_this.state.clear = _this.props.clear;
		return _this;
	}

	_createClass(MxaFwFormPart, [{
		key: 'createFields',
		value: function createFields(data, noData) {
			var fields = [];

			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var tmpRequired = " ";
					var tmpStar = "";

					if (key == "myid") {
						continue;
					}

					if (this.isKey(key) == true) {

						if (noData == false) {
							fields.push(_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement(
									'div',
									{ className: 'control-label col-md-3' },
									_react2.default.createElement(
										_reactEasyForm.Label,
										{ value: key, position: 'before', className: 'mxa_label' },
										_react2.default.createElement(
											'span',
											{ className: 'mxa_span' },
											' * '
										)
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'col-md-5' },
									_react2.default.createElement(_reactEasyForm.TextInput, { name: key, className: 'form-control mxa_form_control', required: true, disabled: true })
								)
							));
						} else {
							fields.push(_react2.default.createElement(
								'div',
								{ className: 'row' },
								_react2.default.createElement(
									'div',
									{ className: 'control-label col-md-3' },
									_react2.default.createElement(
										_reactEasyForm.Label,
										{ value: key, position: 'before', className: 'mxa_label' },
										_react2.default.createElement(
											'span',
											{ className: 'mxa_span' },
											' * '
										)
									)
								),
								_react2.default.createElement(
									'div',
									{ className: 'col-md-5' },
									_react2.default.createElement(_reactEasyForm.TextInput, { name: key, className: 'form-control mxa_form_control', required: true })
								)
							));
						}
					} else {
						fields.push(_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(
								'div',
								{ className: 'control-label col-md-3' },
								_react2.default.createElement(
									_reactEasyForm.Label,
									{ value: key, position: 'before', className: 'mxa_label' },
									_react2.default.createElement('span', { className: 'mxa_span' })
								)
							),
							_react2.default.createElement(
								'div',
								{ className: 'col-md-5' },
								_react2.default.createElement(_reactEasyForm.TextInput, { name: key, className: 'form-control mxa_form_control' })
							)
						));
					}
				}
			}

			return fields;
		}
	}, {
		key: 'createNewData',
		value: function createNewData(stru) {
			var newData = {};
			var index = undefined;

			for (index = 0; index < stru.length; index++) {

				var myField = stru[index];

				for (var key in myField) {
					if (myField.hasOwnProperty(key)) {

						if (key == "Field") {
							var myKey = myField[key];
							newData[myKey] = "";
						}
					}
				}
			}

			return newData;
		}
	}, {
		key: 'isKey',
		value: function isKey(fieldName) {
			var stru = this.props.stru;
			var index = 0;

			for (index = 0; index < stru.length; index++) {

				var myField = stru[index];

				for (var key in myField) {
					if (myField.hasOwnProperty(key)) {

						if (key == "Field") {
							var myKey = myField[key];
							if (myKey == fieldName) {
								var myVal = myField["Key"];
								if (myVal == "PRI") {
									return true;
								} else {
									return false;
								}
							}
						}
					}
				}
			}
			return false;
		}
	}, {
		key: 'handleForm',
		value: function handleForm(data) {

			this.props.handleForm(this.state.step, data);
		}
	}, {
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.clear != this.props.clear) {
				console.log("MXAFWFormPart will receive props ");
				console.log(nextProps);
				this.setState({
					clear: nextProps.clear
				});
			}
		}
	}, {
		key: 'componentWillUpdate',
		value: function componentWillUpdate(nextProps, nextState) {}
	}, {
		key: 'render',
		value: function render() {

			var tmpData = null;
			var noData = false;
			console.log("render MxaFwFormPart");
			console.log(this.state.clear);
			if (this.state.clear == true) {
				tmpData = this.createNewData(this.props.stru);
				noData = true;
			} else {
				if (Object.keys(this.props.data).length == 0) {
					return false;
				}
				tmpData = Object.assign({}, this.createNewData(this.props.stru), this.props.data);
			}

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					_reactEasyForm.Form,
					{ initialData: tmpData, onSubmit: this.handleForm, reload: true, ref: 'mxaFwForm' },
					this.createFields(tmpData, noData)
				)
			);
		}
	}]);

	return MxaFwFormPart;
}(_react2.default.Component);

exports.default = MxaFwFormPart;