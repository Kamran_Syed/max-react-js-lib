'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MxaLanguage = require('./MxaLanguage');

var _MxaLanguage2 = _interopRequireDefault(_MxaLanguage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaHeader = function (_React$Component) {
	_inherits(MxaHeader, _React$Component);

	function MxaHeader(props) {
		_classCallCheck(this, MxaHeader);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaHeader).call(this, props));

		_this.state = {
			language: '',
			mainInfo: null
		};

		return _this;
	}

	_createClass(MxaHeader, [{
		key: 'componentDidMount',
		value: function componentDidMount() {
			console.log("header did mount");
			this.loadData();
		}
	}, {
		key: 'loadData',
		value: function loadData() {
			console.log("header load data");
			var headerUrl = "/api/v1/VMainInfo?vuserid=" + this.props.vUserID + "&CompanyID=" + this.props.vCompanyID + "&authtoken=" + this.props.authToken;

			$.ajax({
				method: "GET",
				url: headerUrl
			}).done(function (data) {
				console.log("header gotdata");
				this.setState({
					mainInfo: data
				});
				this.gotLanguageID(data['LanguageID'], data['MenuID']);
			}.bind(this)).fail(function (msg) {
				console.log("header load data failed " + msg);
			}.bind(this));
		}
	}, {
		key: 'getCompanyNameLogo',
		value: function getCompanyNameLogo() {
			console.log("header getCompanyNameLogo");
			var companyLogoSrc = "data:image/jpg;base64," + this.state.mainInfo.b64CompanyLogo;
			var companyName = this.state.mainInfo.CompanyName;

			return _react2.default.createElement(
				'div',
				{ className: 'MxaHeaderUser' },
				_react2.default.createElement('img', { className: 'img-responsive img-circle', style: { height: '35px', display: 'inline-block' }, src: companyLogoSrc }),
				' ',
				companyName
			);
		}
	}, {
		key: 'getUserPic',
		value: function getUserPic() {
			console.log("header getUserPic");
			var userPicSrc = "data:image/jpg;base64," + this.state.mainInfo.b64UserPicture;
			var userName = this.state.mainInfo.FirstName + ' ' + this.state.mainInfo.LastName;

			return _react2.default.createElement(
				'div',
				{ className: 'img-rounded MxaHeaderUser' },
				userName,
				' ',
				_react2.default.createElement('img', { className: 'img-responsive img-circle', style: { height: '35px', display: 'inline-block' }, src: userPicSrc })
			);
		}
	}, {
		key: 'toggleSideBar',
		value: function toggleSideBar() {
			this.props.toggleSideBar();
		}
	}, {
		key: 'gotLanguageID',
		value: function gotLanguageID(languageID, menuID) {
			this.props.gotLanguageID(languageID, menuID);
		}
	}, {
		key: 'render',
		value: function render() {
			var fillurl = "/api/v1/Language?vuserid=" + this.props.vUserID + "&authtoken=" + this.props.authToken;
			var languageID = '';
			var userNamePic = '';
			var companyNameLogo = '';
			console.log("header render");

			if (this.state.mainInfo != null) {
				console.log('header mainInfo not null');
				languageID = this.state.mainInfo['LanguageID'];
				userNamePic = this.getUserPic();
				companyNameLogo = this.getCompanyNameLogo();
			}

			return _react2.default.createElement(
				'div',
				{ className: 'row MxaHeaderRow' },
				_react2.default.createElement(
					'div',
					{ className: 'col-md-1 text-center MxaHeaderToggle img-rounded', onClick: this.toggleSideBar.bind(this) },
					_react2.default.createElement('img', { className: 'img-responsive', src: '/Content/images/sidebar-toggle-dark.png' })
				),
				_react2.default.createElement('div', { className: 'col-md-1 text-center' }),
				_react2.default.createElement(
					'div',
					{ className: 'col-md-3 text-center' },
					companyNameLogo
				),
				_react2.default.createElement('div', { className: 'col-md-4 text-center' }),
				_react2.default.createElement(
					'div',
					{ className: 'col-md-2 text-center' },
					userNamePic
				),
				_react2.default.createElement(
					'div',
					{ className: 'col-md-1 text-center' },
					_react2.default.createElement(_MxaLanguage2.default, { vUserID: this.props.vUserID, authToken: this.props.authToken, languageID: languageID, fillUrl: fillurl })
				)
			);
		}
	}]);

	return MxaHeader;
}(_react2.default.Component);

exports.default = MxaHeader;