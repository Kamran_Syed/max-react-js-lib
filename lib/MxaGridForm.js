'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MxaWaitLoading = require('./MxaWaitLoading');

var _MxaWaitLoading2 = _interopRequireDefault(_MxaWaitLoading);

var _MxaGrid = require('./MxaGrid');

var _MxaGrid2 = _interopRequireDefault(_MxaGrid);

var _MxaSelect = require('./MxaSelect');

var _MxaSelect2 = _interopRequireDefault(_MxaSelect);

var _MxaFormWizard = require('./MxaFormWizard');

var _MxaFormWizard2 = _interopRequireDefault(_MxaFormWizard);

var _MxaFwButton = require('./MxaFwButton');

var _MxaFwButton2 = _interopRequireDefault(_MxaFwButton);

var _MxaMessage = require('./MxaMessage');

var _MxaMessage2 = _interopRequireDefault(_MxaMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaGridForm = function (_React$Component) {
	_inherits(MxaGridForm, _React$Component);

	function MxaGridForm(props) {
		_classCallCheck(this, MxaGridForm);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaGridForm).call(this, props));

		_this.gotRow = _this.gotRow.bind(_this);
		_this.updateData = _this.updateData.bind(_this);
		_this.deleteData = _this.deleteData.bind(_this);
		_this.ddFillComplete1 = _this.ddFillComplete1.bind(_this);
		_this.ddSelectedVal = _this.ddSelectedVal.bind(_this);
		_this.setWait = _this.setWait.bind(_this);

		_this.state = { stru: [], row: {}, newRow: {}, delRow: {}, gotDdData: false, ddVisible: _this.props.ddVisible, ddSelected: false, dataUrl: _this.props.dataUrl, wait: "full", formVisible: false };
		return _this;
	}

	_createClass(MxaGridForm, [{
		key: 'componentWillUpdate',
		value: function componentWillUpdate(nextProps, nextState) {
			if (nextProps.struUrl != this.props.struUrl || nextProps.dataUrl != this.props.dataUrl) {
				this.state.stru = [];
				this.state.wait = "full";
				this.state.row = {};
				this.state.newRow = {};
				this.state.delRow = {};
				this.state.gotDdData = false;
				this.state.ddSelected = false;
				this.state.ddVisible = nextProps.ddVisible;
				this.state.dataUrl = nextProps.dataUrl;
				this.state.newClick = false;
			}
		}
	}, {
		key: 'gotStru',
		value: function gotStru(tmpStru) {
			this.setState({
				stru: tmpStru
			});
		}
	}, {
		key: 'setWait',
		value: function setWait(value) {
			console.log("Setting Wait " + value);
			this.setState({
				wait: value
			});
		}
	}, {
		key: 'gotRow',
		value: function gotRow(tmpRow) {
			//coming from Grid
			this.setState({
				row: tmpRow,
				formVisible: true
			});
			console.log('got row from grid');
		}
	}, {
		key: 'handleNew',
		value: function handleNew() {
			this.setState({
				newClick: true,
				formVisible: true
			});
		}
	}, {
		key: 'updateData',
		value: function updateData(tmpRow) {
			//coming from form
			console.log('got row from form');
			console.log(tmpRow);
			this.setState({
				newRow: tmpRow
			});
		}
	}, {
		key: 'deleteData',
		value: function deleteData(tmpRow) {
			//coming from form
			console.log('got del row from form');
			console.log(tmpRow);
			this.setState({
				delRow: tmpRow
			});
		}
	}, {
		key: 'ddFillComplete1',
		value: function ddFillComplete1() {
			console.log('dd1 fill comp');
			this.setState({
				gotDdData: true,
				wait: "none"
			});
		}
	}, {
		key: 'ddSelectedVal',
		value: function ddSelectedVal(inputName, value) {
			console.log('ddSelectedVal ' + inputName + ' val=' + value);
			if (this.state.gotDdData == true) {
				this.setState({
					ddSelected: true,
					row: {},
					wait: "half",
					dataUrl: this.props.dataUrl + value
				});
			}
		}
	}, {
		key: 'handleBack',
		value: function handleBack() {
			console.log('form back');
			this.setState({
				formVisible: false
			});
		}
	}, {
		key: 'render',
		value: function render() {

			var tmpRow = this.state.newRow;
			var tmpDelRow = this.state.delRow;

			if (Object.keys(tmpRow).length > 0) {
				this.state.newRow = {};
			}

			if (Object.keys(tmpDelRow).length > 0) {
				this.state.tmpDelRow = {};
			}

			if (this.state.ddVisible == true && this.state.ddSelected == false) {
				return _react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(_MxaWaitLoading2.default, { wait: this.state.wait }),
					_react2.default.createElement(_MxaSelect2.default, { fillUrl: this.props.ddFillUrl1, fillComplete: this.ddFillComplete1, selectedVal: this.ddSelectedVal, label: 'Column Name' })
				);
			} else if (this.state.ddVisible == true && this.state.ddSelected == true) {
				return _react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(_MxaWaitLoading2.default, { wait: this.state.wait }),
					_react2.default.createElement(_MxaSelect2.default, { fillUrl: this.props.ddFillUrl1, fillComplete: this.ddFillComplete1, selectedVal: this.ddSelectedVal, label: 'Column Name' }),
					_react2.default.createElement(_MxaGrid2.default, { struUrl: this.props.struUrl, dataUrl: this.state.dataUrl, gotStru: this.gotStru.bind(this), setWait: this.setWait, gotRow: this.gotRow, newRow: tmpRow, delRow: tmpDelRow, onNew: this.handleNew.bind(this), visible: this.state.formVisible, ref: 'mxaGrid' }),
					_react2.default.createElement(_MxaFormWizard2.default, { steps: this.props.steps, dataUrl: this.state.dataUrl, stru: this.state.stru, row: this.state.row, updateData: this.updateData, deleteData: this.deleteData, setWait: this.setWait, callMaintain: this.props.callMaintain, newClick: this.state.newClick, onBack: this.handleBack.bind(this), visible: this.state.formVisible, ref: 'mxaFormWizard' })
				);
			} else {
				return _react2.default.createElement(
					'div',
					null,
					_react2.default.createElement(_MxaWaitLoading2.default, { wait: this.state.wait }),
					_react2.default.createElement(_MxaGrid2.default, { struUrl: this.props.struUrl, dataUrl: this.state.dataUrl, gotStru: this.gotStru.bind(this), setWait: this.setWait, gotRow: this.gotRow, newRow: tmpRow, delRow: tmpDelRow, onNew: this.handleNew.bind(this), visible: this.state.formVisible, ref: 'mxaGrid' }),
					_react2.default.createElement(_MxaFormWizard2.default, { steps: this.props.steps, dataUrl: this.props.dataUrl, stru: this.state.stru, row: this.state.row, updateData: this.updateData, deleteData: this.deleteData, setWait: this.setWait, callMaintain: this.props.callMaintain, newClick: this.state.newClick, onBack: this.handleBack.bind(this), visible: this.state.formVisible, ref: 'mxaFormWizard' })
				);
			}
		}
	}]);

	return MxaGridForm;
}(_react2.default.Component);

exports.default = MxaGridForm;