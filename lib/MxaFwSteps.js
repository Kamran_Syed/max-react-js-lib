'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaFwSteps = function (_React$Component) {
	_inherits(MxaFwSteps, _React$Component);

	function MxaFwSteps(props) {
		_classCallCheck(this, MxaFwSteps);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaFwSteps).call(this, props));

		_this.state = { steps: _this.props.steps, stepActive: 3, barStyle: {} };
		_this.state.barStyle = _this.setBarStyle();
		return _this;
	}

	_createClass(MxaFwSteps, [{
		key: 'setBarStyle',
		value: function setBarStyle() {
			var barStyle = this.props.styles.barStyle;
			barStyle.width = this.state.stepActive / this.state.steps.length * 100 + '%';

			return barStyle;
		}
	}, {
		key: 'handleBack',
		value: function handleBack() {

			this.props.onBack();
		}
	}, {
		key: 'render',
		value: function render() {
			var barWidth = this.state.stepActive / this.state.steps.length * 100 + '%';
			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ className: 'col-sm-1' },
						_react2.default.createElement('img', { className: 'img-responsive MxaBack', src: '/Content/images/back.png', onClick: this.handleBack.bind(this) })
					),
					_react2.default.createElement(
						'div',
						{ className: 'col-sm-11' },
						_react2.default.createElement(
							'ul',
							{ className: 'mxa_ul' },
							this.state.steps
						)
					)
				),
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ className: 'col-md-12' },
						_react2.default.createElement(
							'div',
							{ className: 'progress' },
							_react2.default.createElement('div', { className: 'progress-bar progress-bar-info progress-bar-striped', role: 'progressbar', 'aria-valuenow': barWidth, 'aria-valuemin': '0', 'aria-valuemax': '100', style: this.state.barStyle })
						)
					)
				)
			);
		}
	}]);

	return MxaFwSteps;
}(_react2.default.Component);

exports.default = MxaFwSteps;