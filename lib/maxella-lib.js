'use strict';

require('babel-polyfill');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactEasyForm = require('react-easy-form');

var _reactBootstrapTable = require('react-bootstrap-table');

var _MxaWaitLoading = require('./MxaWaitLoading');

var _MxaWaitLoading2 = _interopRequireDefault(_MxaWaitLoading);

var _MxaGrid = require('./MxaGrid');

var _MxaGrid2 = _interopRequireDefault(_MxaGrid);

var _MxaSelect = require('./MxaSelect');

var _MxaSelect2 = _interopRequireDefault(_MxaSelect);

var _MxaFwFormPart = require('./MxaFwFormPart');

var _MxaFwFormPart2 = _interopRequireDefault(_MxaFwFormPart);

var _MxaFwFormParts = require('./MxaFwFormParts');

var _MxaFwFormParts2 = _interopRequireDefault(_MxaFwFormParts);

var _MxaFwStep = require('./MxaFwStep');

var _MxaFwStep2 = _interopRequireDefault(_MxaFwStep);

var _MxaFwSteps = require('./MxaFwSteps');

var _MxaFwSteps2 = _interopRequireDefault(_MxaFwSteps);

var _MxaFormWizard = require('./MxaFormWizard');

var _MxaFormWizard2 = _interopRequireDefault(_MxaFormWizard);

var _MxaFwButton = require('./MxaFwButton');

var _MxaFwButton2 = _interopRequireDefault(_MxaFwButton);

var _MxaMessage = require('./MxaMessage');

var _MxaMessage2 = _interopRequireDefault(_MxaMessage);

var _MxaGridForm = require('./MxaGridForm');

var _MxaGridForm2 = _interopRequireDefault(_MxaGridForm);

var _MxaHeader = require('./MxaHeader');

var _MxaHeader2 = _interopRequireDefault(_MxaHeader);

var _MxaFooter = require('./MxaFooter');

var _MxaFooter2 = _interopRequireDefault(_MxaFooter);

var _MxaSideBar = require('./MxaSideBar');

var _MxaSideBar2 = _interopRequireDefault(_MxaSideBar);

var _MxaApp = require('./MxaApp');

var _MxaApp2 = _interopRequireDefault(_MxaApp);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_reactDom2.default.render(_react2.default.createElement(_MxaApp2.default, { authToken: maxellaToken, vUserID: maxellaUserID, vCompanyID: maxellaCompanyID }), mountNode);
//