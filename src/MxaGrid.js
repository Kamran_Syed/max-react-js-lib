import React, { Component, PropTypes } from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';

	class MxaGrid extends React.Component { 
		constructor(props) {
			super(props);
			
			this.getKeys = this.getKeys.bind(this);
			this.getObjIndex = this.getObjIndex.bind(this);
			this.updateData = this.updateData.bind(this);
			
			this.state = {hdrColums : [], gotData : false, stru:[], data:[{id:0,city:"Dublin"}] }
		  }
		
		getJsonData(url){
			$.getJSON(url, function(result){
				//if array add myid to each object
				let tData =[];
				if(Array.isArray(result)){
					result.forEach(function(obj, idx) {
						obj.myid = idx;
						tData.push(obj);
					});
				}
				
				this.setState({
					data: tData,
					gotData: true
				});
				this.props.setWait("none");
				
			}.bind(this));
		}
		
		getJsonStru(struUrl, dataUrl){
			$.getJSON(struUrl, function(result){
				
				var getHeaderColumn = function(item, idx) {
					
				var myret = <TableHeaderColumn dataField={item.Field} isKey={false} dataAlign="left" dataSort={true} key={idx}>{item.Field}</TableHeaderColumn> ;
					
					return  myret;
				}
				
				var headerColumns = result.map(getHeaderColumn);

				var myKeyColumn = <TableHeaderColumn dataField="myid" isKey={true} hidden={true} dataAlign="left" dataSort={true} key={headerColumns.length + 1}>myid</TableHeaderColumn> ;
				headerColumns.push(myKeyColumn);
				
				
				this.setState({
				  hdrColums: headerColumns,
				  gotData: false
				});
				this.state.stru = result;
				this.props.gotStru(result);
				this.getJsonData(dataUrl);

			}.bind(this));
		}
		
		componentDidMount() {
			
			this.getJsonStru(this.props.struUrl, this.props.dataUrl);
			
		}
		
		componentWillUpdate(nextProps, nextState){
			console.log("Grid will update");
			if(nextProps.struUrl != this.props.struUrl){
				console.log("Calling get json Stru");
				this.getJsonStru(nextProps.struUrl, nextProps.dataUrl);
			}else if(nextProps.dataUrl != this.props.dataUrl){
				console.log("Calling get json data");
				this.getJsonData(nextProps.dataUrl);
			}
		}
		
		getKeys(){
			let keys =[];
			this.state.stru.forEach(function(obj) {

				if(obj.Key =="PRI"){
					keys.push(obj.Field);
				}
			});
			
			return keys;
		}
		
		getObjIndex(tobj, keys){

			for(var n=0;n< this.state.data.length;n++) {
				let obj = this.state.data[n];

				switch (keys.length) {
					case 1:
						if(obj[keys[0]] == tobj[keys[0]]){
							return n;
						}
						break;
					case 2:
						if((obj[keys[0]] == tobj[keys[0]]) && (obj[keys[1]] == tobj[keys[1]])){
							return n;
						}
						break;
					case 3:
						if((obj[keys[0]] == tobj[keys[0]]) && (obj[keys[1]] == tobj[keys[1]]) && (obj[keys[2]] == tobj[keys[2]])){
							return n;
						}
						break;
					case 4:
						if((obj[keys[0]] == tobj[keys[0]]) && (obj[keys[1]] == tobj[keys[1]]) && (obj[keys[2]] == tobj[keys[2]]) && (obj[keys[3]] == tobj[keys[3]]) ){
							return n;
						}
						break;
				}
				
			};
			
			return -1;
		}
		
		updateData(tmpRow){
			let keys = this.getKeys();
			let idx = this.getObjIndex(tmpRow, keys);
			
			if(idx != -1){
				this.state.data[idx] = tmpRow;
				console.log("Row updated in grid");
			}else{
				//add new row
				console.log("Row not found in grid. Inserting");
				this.state.data.push(tmpRow);
			}
		}
		
		deleteData(tmpRow){
			let keys = this.getKeys();
			let idx = this.getObjIndex(tmpRow, keys);
			
			if(idx != -1){
				this.state.data.splice(idx, 1);
				console.log("Row deleted in grid");
			}
		}
		
		handleReload(){
			console.log('mxagrid reload by reload button');
			this.props.setWait("half");
			this.getJsonStru(this.props.struUrl, this.props.dataUrl);
		}
		
		handleNew(){
			this.props.onNew();
		}
		
		
		render() {
			
			if(this.props.visible == true){ //form is visible so hide grid
				return false;
			}
			
			var showTbl = {'display':'none'};
			var tableHeaders = [<TableHeaderColumn dataField="id" isKey={true} dataAlign="center" dataSort={true} key="1">id</TableHeaderColumn>,<TableHeaderColumn dataField="city" dataAlign="center" dataSort={true} key="2">city</TableHeaderColumn>];
			
			
			if(this.state.gotData){
				showTbl = {'display':'block'};
				tableHeaders = this.state.hdrColums;
				
				console.log("Grid will render");
				if(Object.keys(this.props.newRow).length > 0){
					console.log("we have new row");
					this.updateData(this.props.newRow);
				}
				
				if(Object.keys(this.props.delRow).length > 0){
					console.log("we have del row");
					this.deleteData(this.props.delRow);
				}
				
			}
			
			
			return (
					<div style={showTbl}>
						<BootstrapTable data={this.state.data} striped={true} hover={true} condensed={true} pagination={true} deleteRow={false} search={true} exportCSV={true} options={{sizePerPageList:[10,15],onRowClick:this.props.gotRow}} selectRow={{mode:"radio",clickToSelect:true,hideSelectColumn:true,bgColor:"#d9edf7"}} onReload={this.handleReload.bind(this)}  onNew={this.handleNew.bind(this)}>
							{tableHeaders}
						</BootstrapTable>
					</div>
				  );
		}
	}
	
export default MxaGrid;