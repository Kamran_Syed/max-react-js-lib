import React, { Component, PropTypes } from 'react';


	class MxaWaitLoading extends React.Component {
		constructor(props) {
			super(props);
		}
		
		render() {
			let clsName="MxaOverlay MxaOverlayFull";
			
			if(this.props.wait == "none"){
				return false;
			}else if(this.props.wait == "half"){
				clsName="MxaOverlay";
			}
			
			return (
				<div className={clsName}>
					<div className="MxaLoadingWait">
						<img className="img-responsive center-block MxaOverlayImg" src="Content/loading.gif" alt="Loading Please wait..." />
					</div>
				</div>
			)
		}
	}
	
export default MxaWaitLoading;