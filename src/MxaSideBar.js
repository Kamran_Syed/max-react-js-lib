import React, { Component, PropTypes } from 'react';
import MxaMenuSection from './MxaMenuSection';

	class MxaSideBar extends React.Component {
		constructor(props) {
			super(props);
			this.state = {menuLoaded: false, menuData: null}
			
			this.handleClick = this.handleClick.bind(this);

		}
		
		componentDidMount() {
			this.loadMenu();
		}
		
		loadMenu(){
			console.log("load menu");
			
			$.ajax({
			  method: "GET",
			  url: this.props.menuUrl
			}).done(function( data ) {
				console.log("menu gotdata");
				this.setState({
					menuData: data,
					menuLoaded: true
				});
				
			}.bind(this)).fail(function( msg ) {
				console.log("load menu failed "+msg);
			}.bind(this));
			
		}
		
		createMenu(){
			let items = [];
			let sections = [];
			let sectionHeader = [];
			let index = 0;
			let sectionData = [];
			
			for	(index = 0; index < this.state.menuData.length; index++) {
				
				let myField = this.state.menuData[index];

				let section = myField['MenuSubArea'];
				let icon = myField['MenuItemIcon'];
				let displayText = myField['DisplayText'];
				
				if(sections.indexOf(section) < 0){
					console.log('New Section='+section);
					
					sections.push(section);
					if(sectionHeader['rk'] != undefined){
						items.push(<MxaMenuSection header={sectionHeader} items={sectionData} showAll={true} key={index} />);
						sectionHeader = [];
						sectionData = [];
						
						sectionHeader['rk'] = index;
						sectionHeader['icon'] = icon;
						sectionHeader['section'] = section;
						
						
					}else{
						//first header
						sectionHeader['rk'] = index;
						sectionHeader['icon'] = icon;
						sectionHeader['section'] = section;
						
					}
					
				}
				
				sectionData.push(<li key={index} onClick={this.handleClick} ><i className={icon}></i>&nbsp;{displayText}</li>);

			}
			
			items.push(<MxaMenuSection header={sectionHeader} items={sectionData} showAll={true} key={index} />);
			
			return items;
		}
		
		handleClick(x){
			let btn = x.target.outerText.toLowerCase();
			console.log(btn);
			this.props.btnClicked(btn);
		}
		
		render() {
					
			if(this.state.menuLoaded == false){
				return false;
			}
			console.log('Sidebar render');
			let menu = this.createMenu();

			return (
					<div className="col-md-3 MxaSideBar">
						{menu}
					</div>
			)
		}
	}	

	
export default MxaSideBar;
