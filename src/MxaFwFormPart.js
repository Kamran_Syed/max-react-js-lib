import "babel-polyfill";
import React, { Component, PropTypes } from 'react';
import {Form, TextInput, Checkbox, Select, Label} from 'react-easy-form';

	class MxaFwFormPart extends React.Component {
		constructor(props) {
			super(props);
			this.handleForm = this.handleForm.bind(this);
			
			this.state = { step:0,active:false, completed:false }
			this.state.clear = this.props.clear;
		}
		
		createFields(data, noData){
			let fields = [];
			
			for (var key in data) {
			  if (data.hasOwnProperty(key)) {
				  let tmpRequired = " ";
				  let tmpStar = "";
				  
				  if(key == "myid"){
					  continue;
				  }
				  
				  if(this.isKey(key) == true){
					  
					  if(noData == false){
						  fields.push(<div className="row"><div className="control-label col-md-3"><Label value={key} position="before" className="mxa_label"><span className="mxa_span"> * </span></Label></div><div className="col-md-5"><TextInput name={key} className="form-control mxa_form_control" required disabled /></div></div>);
					  }else{
						  fields.push(<div className="row"><div className="control-label col-md-3"><Label value={key} position="before" className="mxa_label"><span className="mxa_span"> * </span></Label></div><div className="col-md-5"><TextInput name={key} className="form-control mxa_form_control" required /></div></div>);
					  }
					  
				  }else{
					  fields.push(<div className="row"><div className="control-label col-md-3"><Label value={key} position="before" className="mxa_label"><span className="mxa_span"></span></Label></div><div className="col-md-5"><TextInput name={key} className="form-control mxa_form_control" /></div></div>);
				  }
				
			  }
			}
			
			return fields;
		}
		
		createNewData(stru){
			let newData = {};
			let index;
			
			for	(index = 0; index < stru.length; index++) {
				
				let myField = stru[index];
				
				for (var key in myField) {
				  if (myField.hasOwnProperty(key)) {
					  
					if(key == "Field"){
						let myKey = myField[key];
						newData[myKey] = "";
					}
					
				  }
				}
				
			}
			
			
			return newData;
		}
		
		isKey(fieldName){
			let stru = this.props.stru;
			let index = 0;
			
			for	(index = 0; index < stru.length; index++) {
				
				let myField = stru[index];
				
				for (var key in myField) {
				  if (myField.hasOwnProperty(key)) {
					  
					if(key == "Field"){
						let myKey = myField[key];
						if(myKey == fieldName){
							let myVal = myField["Key"];
							if(myVal == "PRI"){
								return true;
							}else{
								return false;
							}
						}
					}
					
				  }
				}
				
			}
			return false;
		}
		
		handleForm(data){
			
			this.props.handleForm(this.state.step, data);
		}
		componentWillReceiveProps(nextProps){
			if(nextProps.clear != this.props.clear){
				console.log("MXAFWFormPart will receive props ");
				console.log(nextProps);
				this.setState({
					clear: nextProps.clear
				});
			}
		}
		
		componentWillUpdate(nextProps, nextState){
			
		}
		
		
		render() {

			let tmpData = null;
			let noData = false;
			console.log("render MxaFwFormPart");
			console.log(this.state.clear);
			if(this.state.clear == true){
				tmpData = this.createNewData(this.props.stru);
				noData = true;
			}else{
				if (Object.keys(this.props.data).length == 0) {
					return false;
				}
				tmpData = Object.assign({}, this.createNewData(this.props.stru), this.props.data);
				
			}
			
			return (
				<div >
					
					<Form  initialData={tmpData}  onSubmit={this.handleForm} reload={true} ref="mxaFwForm" >

						{this.createFields(tmpData, noData)}
						
						
					</Form>
				</div>
			)
		}
	}
	
export default MxaFwFormPart;