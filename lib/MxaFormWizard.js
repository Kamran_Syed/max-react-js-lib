'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

require('babel-polyfill');

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _MxaFwFormPart = require('./MxaFwFormPart');

var _MxaFwFormPart2 = _interopRequireDefault(_MxaFwFormPart);

var _MxaFwFormParts = require('./MxaFwFormParts');

var _MxaFwFormParts2 = _interopRequireDefault(_MxaFwFormParts);

var _MxaFwStep = require('./MxaFwStep');

var _MxaFwStep2 = _interopRequireDefault(_MxaFwStep);

var _MxaFwSteps = require('./MxaFwSteps');

var _MxaFwSteps2 = _interopRequireDefault(_MxaFwSteps);

var _MxaFwButton = require('./MxaFwButton');

var _MxaFwButton2 = _interopRequireDefault(_MxaFwButton);

var _MxaMessage = require('./MxaMessage');

var _MxaMessage2 = _interopRequireDefault(_MxaMessage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaFormWizard = function (_React$Component) {
	_inherits(MxaFormWizard, _React$Component);

	function MxaFormWizard(props) {
		_classCallCheck(this, MxaFormWizard);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaFormWizard).call(this, props));

		_this.onClickNew = _this.onClickNew.bind(_this);
		_this.onClickUpdate = _this.onClickUpdate.bind(_this);
		_this.onClickDelete = _this.onClickDelete.bind(_this);
		_this.onClickMaintain = _this.onClickMaintain.bind(_this);
		_this.handleForm = _this.handleForm.bind(_this);

		_this.state = { stepObjects: _this.fillStepObjects(1, _this.getStyles()),
			stepActive: 1,
			btnClicked: "none",
			btnLastClicked: "none",
			message: "none",
			messageType: "success",
			clear: false
		};
		if (_this.props.newClick == true) {
			_this.state.btnLastClicked = _this.state.btnClicked;
			_this.state.btnClicked = "new";
			_this.state.message = "none";
			_this.state.clear = true;
		}
		return _this;
	}

	_createClass(MxaFormWizard, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			if (nextProps.newClick != this.props.newClick) {
				console.log("MxaFormWizard.js will receive props ");

				this.setState({
					btnLastClicked: this.state.btnClicked,
					btnClicked: "new",
					message: "none",
					clear: true
				});
			}
		}
	}, {
		key: 'componentWillUpdate',
		value: function componentWillUpdate(nextProps, nextState) {}
	}, {
		key: 'getStyles',
		value: function getStyles() {
			var styles = {};
			styles.stepText = {
				color: '#333'
			};

			styles.stepNumber = {
				color: 'blue'
			};

			styles.barStyle = {
				//height:'5px',
				width: '25%'
			};

			return styles;
		}
	}, {
		key: 'fillStepObjects',
		value: function fillStepObjects(stepActive, styles) {
			var stepObjects = [];

			this.props.steps.forEach(function (entry) {
				var isActive = false;

				if (entry.step <= stepActive) {
					isActive = true;
				}

				stepObjects.push(_react2.default.createElement(_MxaFwStep2.default, { step: entry.step, stepText: entry.stepText, active: isActive, styles: styles }));
			});

			return stepObjects;
		}
	}, {
		key: 'handleForm',
		value: function handleForm(step, data) {
			console.log(this.state.btnClicked + this.state.btnLastClicked);

			if (this.state.btnClicked == "update" && this.state.btnLastClicked != "new") {
				console.log("update click");
				this.updateApi(data);
			} else if (this.state.btnClicked == "update" && this.state.btnLastClicked == "new") {
				console.log("insert click");
				this.insertApi(data);
			} else if (this.state.btnClicked == "delete") {
				console.log("delete click");
				this.deleteApi(data);
			}
		}
	}, {
		key: 'insertApi',
		value: function insertApi(arrData) {
			this.props.setWait("half");
			this.state.message = "none";
			var tdata = {};
			console.log("tdata");

			Object.keys(arrData).forEach(function (key) {
				tdata[key] = arrData[key];
			});

			console.log(tdata);
			$.ajax({
				method: "POST",
				url: this.props.dataUrl,
				data: tdata
			}).done(function (msg) {
				this.props.setWait("none");
				this.props.updateData(tdata);

				this.setState({
					messageType: "success",
					message: "Data Saved",
					clear: true
				});
			}.bind(this)).fail(function (msg) {
				//do not clear form
				this.props.setWait("none");
				this.setState({
					messageType: "error",
					message: "Cannot Save Data",
					btnClicked: "new",
					clear: false
				});
			}.bind(this));
		}
	}, {
		key: 'updateApi',
		value: function updateApi(tdata) {
			this.props.setWait("half");
			this.state.message = "none";
			$.ajax({
				method: "PUT",
				url: this.props.dataUrl,
				contentType: "application/json",
				data: tdata
			}).done(function (msg) {
				this.props.setWait("none");
				this.props.updateData(tdata);

				this.setState({
					messageType: "success",
					message: "Data Saved",
					clear: true
				});
			}.bind(this)).fail(function (msg) {
				//do not clear form
				this.props.setWait("none");
				this.setState({
					messageType: "error",
					message: "Cannot Save Data"
				});
			}.bind(this));
		}
	}, {
		key: 'deleteApi',
		value: function deleteApi(tdata) {
			this.props.setWait("half");
			this.state.message = "none";
			$.ajax({
				method: "DELETE",
				url: this.props.dataUrl,
				contentType: "application/json",
				data: tdata
			}).done(function (msg) {
				this.props.setWait("none");
				this.props.deleteData(tdata);

				this.setState({
					messageType: "success",
					message: "Data Deleted",
					clear: true
				});
			}.bind(this)).fail(function (msg) {
				//do not clear form
				this.props.setWait("none");
				this.setState({
					messageType: "error",
					message: "Cannot Delete Data"
				});
			}.bind(this));
		}
	}, {
		key: 'onClickNew',
		value: function onClickNew() {
			this.state.btnLastClicked = this.state.btnClicked;
			this.state.btnClicked = "new";
			this.state.message = "none";
			this.setState({
				clear: true
			});
		}
	}, {
		key: 'onClickUpdate',
		value: function onClickUpdate() {
			this.state.btnLastClicked = this.state.btnClicked;
			this.state.btnClicked = "update";
			this.refs.mxaFwFormPart.refs.mxaFwForm.submit();
		}
	}, {
		key: 'onClickDelete',
		value: function onClickDelete() {
			this.state.btnLastClicked = this.state.btnClicked;
			this.state.btnClicked = "delete";
			this.refs.mxaFwFormPart.refs.mxaFwForm.submit();
		}
	}, {
		key: 'onClickMaintain',
		value: function onClickMaintain() {
			console.log("maintain click");
			this.props.callMaintain();
		}
	}, {
		key: 'componentWillUpdate',
		value: function componentWillUpdate(nextProps, nextState) {
			console.log('MxaFormWizard will update');
			if (nextProps.row != this.props.row) {
				console.log('MxaFormWizard state reset');
				this.state.message = "none";
				this.state.btnClicked = "none";
				this.state.btnLastClicked = "none";
			}
		}
	}, {
		key: 'handleBack',
		value: function handleBack() {
			this.props.onBack();
		}
	}, {
		key: 'render',
		value: function render() {
			var myClear = false;

			if (this.props.visible == false) {
				return false;
			}

			if (this.state.clear == true) {
				myClear = true;
				this.state.clear = false;
			}

			return _react2.default.createElement(
				'div',
				null,
				_react2.default.createElement(_MxaFwSteps2.default, { steps: this.state.stepObjects, styles: this.getStyles(), onBack: this.handleBack.bind(this) }),
				_react2.default.createElement(_MxaMessage2.default, { message: this.state.message, interval: '3000', type: this.state.messageType }),
				_react2.default.createElement(_MxaFwFormPart2.default, { data: this.props.row, stru: this.props.stru, handleForm: this.handleForm, clear: myClear, ref: 'mxaFwFormPart' }),
				_react2.default.createElement(
					'div',
					{ className: 'row' },
					_react2.default.createElement(
						'div',
						{ className: 'col-md-2' },
						_react2.default.createElement(_MxaFwButton2.default, { btnName: 'New', handleClick: this.onClickNew, color: "btn btn-success", ref: 'btnNew' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'col-md-2' },
						_react2.default.createElement(_MxaFwButton2.default, { btnName: 'Update', handleClick: this.onClickUpdate, color: "btn btn-info", ref: 'btnUpdate' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'col-md-2' },
						_react2.default.createElement(_MxaFwButton2.default, { btnName: 'Delete', handleClick: this.onClickDelete, color: "btn btn-danger", ref: 'btnDelete' })
					),
					_react2.default.createElement(
						'div',
						{ className: 'col-md-2' },
						_react2.default.createElement(_MxaFwButton2.default, { btnName: 'Maintain Text', handleClick: this.onClickMaintain, color: "btn btn-warning" })
					)
				)
			);
		}
	}]);

	return MxaFormWizard;
}(_react2.default.Component);

exports.default = MxaFormWizard;