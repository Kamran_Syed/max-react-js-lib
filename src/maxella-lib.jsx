import 'babel-polyfill';
import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Form, TextInput, Checkbox, Select, Label} from 'react-easy-form';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
//
import MxaWaitLoading from './MxaWaitLoading';
import MxaGrid from './MxaGrid';
import MxaSelect from './MxaSelect';
import MxaFwFormPart from './MxaFwFormPart';
import MxaFwFormParts from './MxaFwFormParts';
import MxaFwStep from './MxaFwStep';
import MxaFwSteps from './MxaFwSteps';
import MxaFormWizard from './MxaFormWizard';
import MxaFwButton from './MxaFwButton';
import MxaMessage from './MxaMessage';
import MxaGridForm from './MxaGridForm';
import MxaHeader from './MxaHeader';
import MxaFooter from './MxaFooter';
import MxaSideBar from './MxaSideBar';
import MxaApp from './MxaApp';

ReactDOM.render(<MxaApp authToken={maxellaToken} vUserID={maxellaUserID} vCompanyID={maxellaCompanyID} />, mountNode);
