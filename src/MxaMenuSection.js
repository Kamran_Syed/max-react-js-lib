import React, { Component, PropTypes } from 'react';

	class MxaMenuSection extends React.Component {
		constructor(props) {
			super(props);
			this.state = {showAll: this.props.showAll, header: this.props.header, items: this.props.items}
			
			this.localClick = this.localClick.bind(this);

		}
		
		componentWillReceiveProps(nextProps){
			this.setState({
				showAll: nextProps.showAll,
				header: nextProps.header,
				items: nextProps.items
			});
		}
		
				
		createMenu(){
			let items = [];
			let hdr = this.state.header;
			let arrowRight = "fa fa-angle-left MxaRight";
			let arrowDown = "fa fa-angle-down MxaRight";
			let arrow = arrowRight;
			let highlight = "MxaSideBarSelected";
			console.log('menu section hdr coming next');
			
			if(this.state.showAll == true){
				arrow = arrowDown;
			}else{
				arrow = arrowRight;
				highlight = 'None';
			}
			
			let newhdr = <li key={hdr['rk']} className={highlight} onClick={this.localClick} ><i className={hdr['icon']}></i>&nbsp;{hdr['section']}<i className={arrow}></i></li>;
			
			items.push(newhdr);
			if(this.state.showAll){
				items.push(this.state.items);
			}
			
			return items;
		}
		
		localClick(x){
			console.log('menu local  click');
			if(this.state.showAll == true){
				this.setState({
					showAll:false
				});
			}else{
				this.setState({
					showAll:true
				});
			}
		}
		
		render() {
			
			let menu = this.createMenu();
			
			return (
				<ul>{menu}</ul>
			)
		}
	}	

	
export default MxaMenuSection;
