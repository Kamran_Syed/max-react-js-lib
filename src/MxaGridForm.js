import React, { Component, PropTypes } from 'react';
import MxaWaitLoading from './MxaWaitLoading';
import MxaGrid from './MxaGrid';
import MxaSelect from './MxaSelect';
import MxaFormWizard from './MxaFormWizard';
import MxaFwButton from './MxaFwButton';
import MxaMessage from './MxaMessage';


	class MxaGridForm extends React.Component {
		constructor(props) {
			super(props);

			this.gotRow = this.gotRow.bind(this);
			this.updateData = this.updateData.bind(this);
			this.deleteData = this.deleteData.bind(this);
			this.ddFillComplete1 = this.ddFillComplete1.bind(this);
			this.ddSelectedVal = this.ddSelectedVal.bind(this);
			this.setWait = this.setWait.bind(this);
			
			this.state = {stru:[], row:{}, newRow:{}, delRow:{}, gotDdData:false, ddVisible:this.props.ddVisible, ddSelected:false, dataUrl:this.props.dataUrl, wait:"full", formVisible:false }
		}
		
		componentWillUpdate(nextProps, nextState){
			if(nextProps.struUrl != this.props.struUrl || nextProps.dataUrl != this.props.dataUrl){
				this.state.stru = [];
				this.state.wait = "full";
				this.state.row = {};
				this.state.newRow = {};
				this.state.delRow = {};
				this.state.gotDdData = false;
				this.state.ddSelected = false;
				this.state.ddVisible = nextProps.ddVisible;
				this.state.dataUrl = nextProps.dataUrl;
				this.state.newClick = false;

			}
		}
		
		gotStru(tmpStru){
			this.setState({
			  stru: tmpStru
			});
			
		}
		
		setWait(value){
			console.log("Setting Wait "+value);
			this.setState({
			  wait: value
			});
			
		}
		
		gotRow(tmpRow){
			//coming from Grid
			this.setState({
			  row: tmpRow,
			  formVisible: true
			});
			console.log('got row from grid');
		}
		
		handleNew(){
			this.setState({
			  newClick: true,
			  formVisible: true
			});
		}
		
		updateData(tmpRow){
			//coming from form
			console.log('got row from form');
			console.log(tmpRow);
			this.setState({
			  newRow: tmpRow
			});
		}
		
		deleteData(tmpRow){
			//coming from form
			console.log('got del row from form');
			console.log(tmpRow);
			this.setState({
			  delRow: tmpRow
			});
		}
		
		ddFillComplete1(){
			console.log('dd1 fill comp');
			this.setState({
			  gotDdData: true,
			  wait: "none"
			});
		}
		
		ddSelectedVal(inputName, value){
			console.log('ddSelectedVal '+inputName +' val='+value);
			if(this.state.gotDdData == true){
				this.setState({
				  ddSelected: true,
				  row: {},
				  wait: "half",
				  dataUrl: this.props.dataUrl +value
				}); 
			}
		}
		
		handleBack(){
			console.log('form back');
			this.setState({
			  formVisible: false
			});
		}
		
		
		render() {
			
			let tmpRow = this.state.newRow;
			let tmpDelRow = this.state.delRow;
			
			if(Object.keys(tmpRow).length > 0){
				this.state.newRow = {};
			}
			
			if(Object.keys(tmpDelRow).length > 0){
				this.state.tmpDelRow = {};
			}
			
			if(this.state.ddVisible == true && this.state.ddSelected == false){
				return (
					<div>
						<MxaWaitLoading wait={this.state.wait}/>
						<MxaSelect fillUrl={this.props.ddFillUrl1} fillComplete={this.ddFillComplete1} selectedVal={this.ddSelectedVal} label="Column Name" />
					</div>
				)
			}else if(this.state.ddVisible == true && this.state.ddSelected == true){
				return (
					<div>
						<MxaWaitLoading wait={this.state.wait}/>
						<MxaSelect fillUrl={this.props.ddFillUrl1} fillComplete={this.ddFillComplete1} selectedVal={this.ddSelectedVal} label="Column Name" />
						<MxaGrid  struUrl={this.props.struUrl} dataUrl={this.state.dataUrl} gotStru={this.gotStru.bind(this)} setWait={this.setWait} gotRow={this.gotRow} newRow={tmpRow} delRow={tmpDelRow} onNew={this.handleNew.bind(this)} visible={this.state.formVisible}  ref="mxaGrid"/>
						
						<MxaFormWizard  steps={this.props.steps} dataUrl={this.state.dataUrl} stru={this.state.stru} row={this.state.row} updateData={this.updateData} deleteData={this.deleteData} setWait={this.setWait} callMaintain={this.props.callMaintain} newClick={this.state.newClick} onBack={this.handleBack.bind(this)} visible={this.state.formVisible} ref="mxaFormWizard" />
					</div>
				)
			}else{
				return (
					<div>
						<MxaWaitLoading wait={this.state.wait}/>
						<MxaGrid  struUrl={this.props.struUrl} dataUrl={this.state.dataUrl} gotStru={this.gotStru.bind(this)} setWait={this.setWait} gotRow={this.gotRow} newRow={tmpRow} delRow={tmpDelRow} onNew={this.handleNew.bind(this)} visible={this.state.formVisible}  ref="mxaGrid"/>
						
						<MxaFormWizard  steps={this.props.steps} dataUrl={this.props.dataUrl} stru={this.state.stru} row={this.state.row} updateData={this.updateData} deleteData={this.deleteData} setWait={this.setWait} callMaintain={this.props.callMaintain} newClick={this.state.newClick} onBack={this.handleBack.bind(this)} visible={this.state.formVisible} ref="mxaFormWizard" />
					</div>
				)
			}
			
		}
	}

	
export default MxaGridForm;