"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaMessage = function (_React$Component) {
	_inherits(MxaMessage, _React$Component);

	function MxaMessage(props) {
		_classCallCheck(this, MxaMessage);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaMessage).call(this, props));

		_this.state = { visible: true };
		return _this;
	}

	_createClass(MxaMessage, [{
		key: "componentDidMount",
		value: function componentDidMount() {
			this.startTimer();
		}
	}, {
		key: "componentWillUpdate",
		value: function componentWillUpdate(nextProps, nextState) {
			if (nextProps.type != this.props.type || nextProps.message != this.props.message) {
				this.state.visible = true;
				this.startTimer();
			}
		}
	}, {
		key: "startTimer",
		value: function startTimer() {
			var self = this;

			setTimeout(function () {
				self.hide();
			}, this.props.interval);
		}
	}, {
		key: "hide",
		value: function hide() {
			this.setState({
				visible: false
			});
		}
	}, {
		key: "render",
		value: function render() {
			var styleCss = "MxaMessageSuccess";

			if (this.props.message == "none") {
				return false;
			}

			if (this.state.visible == true) {
				if (this.props.type == "error") {
					styleCss = "MxaMessageError";
				} else if (this.props.type == "warning") {
					styleCss = "MxaMessageWarning";
				} else if (this.props.type == "success") {
					styleCss = "MxaMessageSuccess";
				}
			} else {
				if (this.props.type == "error") {
					styleCss = "MxaMessageError fade";
				} else if (this.props.type == "warning") {
					styleCss = "MxaMessageWarning fade";
				} else if (this.props.type == "success") {
					styleCss = "MxaMessageSuccess fade";
				}
			}

			return _react2.default.createElement(
				"div",
				{ className: styleCss },
				this.props.message
			);
		}
	}]);

	return MxaMessage;
}(_react2.default.Component);

exports.default = MxaMessage;