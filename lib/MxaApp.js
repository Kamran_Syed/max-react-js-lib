'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactEasyForm = require('react-easy-form');

var _reactBootstrapTable = require('react-bootstrap-table');

var _reactBootstrap = require('react-bootstrap');

var _MxaWaitLoading = require('./MxaWaitLoading');

var _MxaWaitLoading2 = _interopRequireDefault(_MxaWaitLoading);

var _MxaGrid = require('./MxaGrid');

var _MxaGrid2 = _interopRequireDefault(_MxaGrid);

var _MxaSelect = require('./MxaSelect');

var _MxaSelect2 = _interopRequireDefault(_MxaSelect);

var _MxaFwFormPart = require('./MxaFwFormPart');

var _MxaFwFormPart2 = _interopRequireDefault(_MxaFwFormPart);

var _MxaFwFormParts = require('./MxaFwFormParts');

var _MxaFwFormParts2 = _interopRequireDefault(_MxaFwFormParts);

var _MxaFwStep = require('./MxaFwStep');

var _MxaFwStep2 = _interopRequireDefault(_MxaFwStep);

var _MxaFwSteps = require('./MxaFwSteps');

var _MxaFwSteps2 = _interopRequireDefault(_MxaFwSteps);

var _MxaFormWizard = require('./MxaFormWizard');

var _MxaFormWizard2 = _interopRequireDefault(_MxaFormWizard);

var _MxaFwButton = require('./MxaFwButton');

var _MxaFwButton2 = _interopRequireDefault(_MxaFwButton);

var _MxaMessage = require('./MxaMessage');

var _MxaMessage2 = _interopRequireDefault(_MxaMessage);

var _MxaGridForm = require('./MxaGridForm');

var _MxaGridForm2 = _interopRequireDefault(_MxaGridForm);

var _MxaHeader = require('./MxaHeader');

var _MxaHeader2 = _interopRequireDefault(_MxaHeader);

var _MxaFooter = require('./MxaFooter');

var _MxaFooter2 = _interopRequireDefault(_MxaFooter);

var _MxaSideBar = require('./MxaSideBar');

var _MxaSideBar2 = _interopRequireDefault(_MxaSideBar);

var _MxaLanguage = require('./MxaLanguage');

var _MxaLanguage2 = _interopRequireDefault(_MxaLanguage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MxaApp = function (_React$Component) {
	_inherits(MxaApp, _React$Component);

	function MxaApp(props) {
		_classCallCheck(this, MxaApp);

		var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(MxaApp).call(this, props));

		_this.btnClicked = _this.btnClicked.bind(_this);
		_this.callMaintain = _this.callMaintain.bind(_this);
		_this.gotLanguageID = _this.gotLanguageID.bind(_this);
		_this.toggleSideBar = _this.toggleSideBar.bind(_this);

		_this.state = {
			showGridForm: "none",
			languageID: "EN",
			menuID: "M1",
			sideBar: "open"
		};
		return _this;
	}

	_createClass(MxaApp, [{
		key: 'componentDidMount',
		value: function componentDidMount() {}
	}, {
		key: 'btnClicked',
		value: function btnClicked(btn) {
			console.log("MaxApp btn " + btn);
			switch (btn) {
				case "data dictionary":
					this.setState({
						showGridForm: "ddmain"
					});
					break;
				case "data dictionary texts":
					this.setState({
						showGridForm: "ddtexts"
					});
					break;
				case "transactions main":
					this.setState({
						showGridForm: "txmain"
					});
					break;
				case "transactions details":
					this.setState({
						showGridForm: "txdetails"
					});
					break;

			};
		}
	}, {
		key: 'callMaintain',
		value: function callMaintain() {
			this.btnClicked("data dictionary texts");
		}
	}, {
		key: 'toggleSideBar',
		value: function toggleSideBar() {
			if (this.state.sideBar == "open") {
				this.setState({
					sideBar: "close"
				});
			} else {
				this.setState({
					sideBar: "open"
				});
			}
		}
	}, {
		key: 'gotLanguageID',
		value: function gotLanguageID(LanguageID, MenuID) {
			this.setState({
				languageID: LanguageID,
				menuID: MenuID
			});
		}
	}, {
		key: 'render',
		value: function render() {
			var surl = undefined;
			var durl = undefined;
			var fillurl = undefined;
			var sideBarUrl = "/api/v1/objfactory?vuserid=" + this.props.vUserID + "&CompanyID=" + this.props.vCompanyID + "&authtoken=" + this.props.authToken + "&LanguageID=" + this.state.languageID + "&MenuID=" + this.state.menuID + "&TransactionID=CFG_MENUITEMS_DISPLAY";

			switch (this.state.showGridForm) {
				case "none":
					return _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_MxaHeader2.default, { vUserID: this.props.vUserID, vCompanyID: this.props.vCompanyID, authToken: this.props.authToken, gotLanguageID: this.gotLanguageID, toggleSideBar: this.toggleSideBar }),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(_MxaSideBar2.default, { btnClicked: this.btnClicked, menuUrl: sideBarUrl }),
							_react2.default.createElement('div', { className: 'col-md-8 gridForm' })
						),
						_react2.default.createElement(_MxaFooter2.default, null)
					);
					break;
				case "txdetails":
					surl = "/api/v1/txdetailsStru?vuserid=jon&authtoken=" + this.props.authToken;
					durl = "/api/v1/TxDetailsData?vuserid=jon&authtoken=" + this.props.authToken + "&TransactionID=";
					fillurl = "/api/v1/txdetailsStru?TransactionID=true&vuserid=jon&authtoken=" + this.props.authToken;

					return _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_MxaHeader2.default, { vUserID: this.props.vUserID, vCompanyID: this.props.vCompanyID, authToken: this.props.authToken, gotLanguageID: this.gotLanguageID, toggleSideBar: this.toggleSideBar }),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(_MxaSideBar2.default, { btnClicked: this.btnClicked, menuUrl: sideBarUrl }),
							_react2.default.createElement(
								'div',
								{ className: 'col-md-8 gridForm' },
								_react2.default.createElement(_MxaGridForm2.default, { struUrl: surl, dataUrl: durl, steps: [{ step: 1, stepText: "Step One" }], ddVisible: true, ddFillUrl1: fillurl, callMaintain: this.callMaintain })
							)
						),
						_react2.default.createElement(_MxaFooter2.default, null)
					);
					break;

				case "ddtexts":
					surl = "/api/v1/DdTextsStru?vuserid=jon&authtoken=" + this.props.authToken;
					durl = "/api/v1/DdTextsData?vuserid=jon&authtoken=" + this.props.authToken + "&ColumnName=";
					fillurl = "/api/v1/DdTextsStru?ColumnNames=true&vuserid=jon&authtoken=" + this.props.authToken;

					return _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_MxaHeader2.default, { vUserID: this.props.vUserID, vCompanyID: this.props.vCompanyID, authToken: this.props.authToken, gotLanguageID: this.gotLanguageID, toggleSideBar: this.toggleSideBar }),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(_MxaSideBar2.default, { btnClicked: this.btnClicked, menuUrl: sideBarUrl }),
							_react2.default.createElement(
								'div',
								{ className: 'col-md-8 gridForm' },
								_react2.default.createElement(_MxaGridForm2.default, { struUrl: surl, dataUrl: durl, steps: [{ step: 1, stepText: "Step One" }], ddVisible: true, ddFillUrl1: fillurl, callMaintain: this.callMaintain })
							)
						),
						_react2.default.createElement(_MxaFooter2.default, null)
					);
					break;

				case "ddmain":
					surl = "/api/v1/DdMainStru?vuserid=jon&authtoken=" + this.props.authToken;
					durl = "/api/v1/DdMainData?vuserid=jon&authtoken=" + this.props.authToken;

					return _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_MxaHeader2.default, { vUserID: this.props.vUserID, vCompanyID: this.props.vCompanyID, authToken: this.props.authToken, gotLanguageID: this.gotLanguageID, toggleSideBar: this.toggleSideBar }),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(_MxaSideBar2.default, { btnClicked: this.btnClicked, menuUrl: sideBarUrl }),
							_react2.default.createElement(
								'div',
								{ className: 'col-md-8 gridForm' },
								_react2.default.createElement(_MxaGridForm2.default, { struUrl: surl, dataUrl: durl, steps: [{ step: 1, stepText: "Step One" }], ddVisible: false, ddFillUrl1: 'none', callMaintain: this.callMaintain })
							)
						),
						_react2.default.createElement(_MxaFooter2.default, null)
					);
					break;

				case "txmain":
					surl = "/api/v1/TxMainStru?vuserid=jon&authtoken=" + this.props.authToken;
					durl = "/api/v1/TxMainData?vuserid=jon&authtoken=" + this.props.authToken;

					return _react2.default.createElement(
						'div',
						null,
						_react2.default.createElement(_MxaHeader2.default, { vUserID: this.props.vUserID, vCompanyID: this.props.vCompanyID, authToken: this.props.authToken, gotLanguageID: this.gotLanguageID, toggleSideBar: this.toggleSideBar }),
						_react2.default.createElement(
							'div',
							{ className: 'row' },
							_react2.default.createElement(_MxaSideBar2.default, { btnClicked: this.btnClicked, menuUrl: sideBarUrl }),
							_react2.default.createElement(
								'div',
								{ className: 'col-md-8 gridForm' },
								_react2.default.createElement(_MxaGridForm2.default, { struUrl: surl, dataUrl: durl, steps: [{ step: 1, stepText: "Step One" }], ddVisible: false, ddFillUrl1: 'none', callMaintain: this.callMaintain })
							)
						),
						_react2.default.createElement(_MxaFooter2.default, null)
					);
					break;

			};
		}
	}]);

	return MxaApp;
}(_react2.default.Component);

exports.default = MxaApp;