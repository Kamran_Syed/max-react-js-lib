import React, { Component, PropTypes } from 'react';
import {Form, TextInput, Checkbox, Select, Label} from 'react-easy-form';

	class MxaSelect extends React.Component {
		constructor(props) {
			super(props);
			this.state = {formData:{}, data:{} }
		}
		
		updateFormData(inputName, value) {

			let formData = this.state.formData;
			formData[inputName] = value;
			this.state.formData = formData;
			this.setState({
				formData
			});
			this.props.selectedVal(inputName, value);
		}
		
		getFormData(inputName) {
			return this.state.formData[inputName];
		}

		getChildContext() {
			return {
				updateFormData: this.updateFormData.bind(this),
				getFormData: this.getFormData.bind(this)
			};
		}
		
		componentDidMount() {
			$.getJSON(this.props.fillUrl, function(result){
				this.props.fillComplete();
				this.setState({
				  data: result
				});

			}.bind(this));
		}

		render() {
			
			return (
					<Label value={this.props.label} position="before">
						<Select name="select1" values={this.state.data} />
					</Label>
			)
		}
	}
	
	MxaSelect.childContextTypes = {
		updateFormData: React.PropTypes.func,
		getFormData: React.PropTypes.func
	};
	
export default MxaSelect;