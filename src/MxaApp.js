import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Form, TextInput, Checkbox, Select, Label} from 'react-easy-form';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';
import { Button } from 'react-bootstrap';

import MxaWaitLoading from './MxaWaitLoading';
import MxaGrid from './MxaGrid';
import MxaSelect from './MxaSelect';
import MxaFwFormPart from './MxaFwFormPart';
import MxaFwFormParts from './MxaFwFormParts';
import MxaFwStep from './MxaFwStep';
import MxaFwSteps from './MxaFwSteps';
import MxaFormWizard from './MxaFormWizard';
import MxaFwButton from './MxaFwButton';
import MxaMessage from './MxaMessage';
import MxaGridForm from './MxaGridForm';
import MxaHeader from './MxaHeader';
import MxaFooter from './MxaFooter';
import MxaSideBar from './MxaSideBar';
import MxaLanguage from './MxaLanguage';

	class MxaApp extends React.Component {
		constructor(props) {
			super(props);
			this.btnClicked = this.btnClicked.bind(this);
			this.callMaintain = this.callMaintain.bind(this);
			this.gotLanguageID = this.gotLanguageID.bind(this);
			this.toggleSideBar = this.toggleSideBar.bind(this);
			
			this.state = {
					showGridForm: "none",
					languageID: "EN",
					menuID: "M1",
					sideBar: "open"
			}
		}
		
		componentDidMount() {
			
		}
		
		btnClicked(btn){
			console.log("MaxApp btn "+btn);
			switch (btn) {
					case "data dictionary":
						this.setState({
							showGridForm: "ddmain"
						});
						break;
					case "data dictionary texts":
						this.setState({
							showGridForm: "ddtexts"
						});
						break;
					case "transactions main":
						this.setState({
							showGridForm: "txmain"
						});
						break;
					case "transactions details":
						this.setState({
							showGridForm: "txdetails"
						});
						break;

			};
				
		}
		
		callMaintain(){
			this.btnClicked("data dictionary texts");
		}
		
		toggleSideBar(){
			if(this.state.sideBar == "open"){
				this.setState({
					sideBar: "close"
				});
			}else{
				this.setState({
					sideBar: "open"
				});
			}
		}
		
		gotLanguageID(LanguageID, MenuID){
			this.setState({
				languageID: LanguageID,
				menuID: MenuID
			});
		}
		
		
		render() {
					let surl;
					let durl;
					let fillurl;
					let sideBarUrl = "/api/v1/objfactory?vuserid=" + this.props.vUserID + "&CompanyID=" + this.props.vCompanyID + "&authtoken=" + this.props.authToken + "&LanguageID=" + this.state.languageID + "&MenuID=" + this.state.menuID + "&TransactionID=CFG_MENUITEMS_DISPLAY";
	
			switch(this.state.showGridForm){
				case "none":
					return (
							<div>
								<MxaHeader vUserID={this.props.vUserID} vCompanyID={this.props.vCompanyID} authToken={this.props.authToken} gotLanguageID={this.gotLanguageID} toggleSideBar={this.toggleSideBar} />
								<div className="row">
									<MxaSideBar btnClicked={this.btnClicked} menuUrl={sideBarUrl} />
									<div className ="col-md-8 gridForm">
										
									</div>
								</div>
								<MxaFooter />
							</div>
					)
					break;
				case "txdetails":
					surl = "/api/v1/txdetailsStru?vuserid=jon&authtoken=" + this.props.authToken;
					durl = "/api/v1/TxDetailsData?vuserid=jon&authtoken=" + this.props.authToken + "&TransactionID=";
					fillurl = "/api/v1/txdetailsStru?TransactionID=true&vuserid=jon&authtoken=" + this.props.authToken;
					
					return (
							<div>
								<MxaHeader vUserID={this.props.vUserID} vCompanyID={this.props.vCompanyID} authToken={this.props.authToken} gotLanguageID={this.gotLanguageID} toggleSideBar={this.toggleSideBar} />
								<div className="row">
									<MxaSideBar btnClicked={this.btnClicked} menuUrl={sideBarUrl} />
									<div className ="col-md-8 gridForm">
										<MxaGridForm  struUrl= {surl} dataUrl={durl}  steps={[{step:1,stepText:"Step One"}]} ddVisible={true} ddFillUrl1={fillurl} callMaintain={this.callMaintain} />
									</div>
								</div>
								<MxaFooter />
							</div>
					)
					break;

				case "ddtexts":
					surl = "/api/v1/DdTextsStru?vuserid=jon&authtoken="+ this.props.authToken ;
					durl = "/api/v1/DdTextsData?vuserid=jon&authtoken="+ this.props.authToken +"&ColumnName=";
					fillurl = "/api/v1/DdTextsStru?ColumnNames=true&vuserid=jon&authtoken="+ this.props.authToken;
					
					return (
							<div>
								<MxaHeader vUserID={this.props.vUserID} vCompanyID={this.props.vCompanyID} authToken={this.props.authToken} gotLanguageID={this.gotLanguageID} toggleSideBar={this.toggleSideBar} />
								<div className="row">
									<MxaSideBar btnClicked={this.btnClicked} menuUrl={sideBarUrl} />
									<div className ="col-md-8 gridForm">
										<MxaGridForm  struUrl={surl} dataUrl={durl}  steps={[{step:1,stepText:"Step One"}]} ddVisible={true} ddFillUrl1={fillurl} callMaintain={this.callMaintain} />
									</div>
								</div>
								<MxaFooter />
							</div>
					)
					break;
					
				case "ddmain":
					surl = "/api/v1/DdMainStru?vuserid=jon&authtoken="+ this.props.authToken;
					durl = "/api/v1/DdMainData?vuserid=jon&authtoken="+ this.props.authToken;
					
					return (
							<div>
								<MxaHeader vUserID={this.props.vUserID} vCompanyID={this.props.vCompanyID} authToken={this.props.authToken} gotLanguageID={this.gotLanguageID} toggleSideBar={this.toggleSideBar} />
								<div className="row">
									<MxaSideBar btnClicked={this.btnClicked} menuUrl={sideBarUrl} />
									<div className ="col-md-8 gridForm">
										<MxaGridForm  struUrl={surl} dataUrl={durl}  steps={[{step:1,stepText:"Step One"}]} ddVisible={false} ddFillUrl1="none" callMaintain={this.callMaintain} />
									</div>
								</div>
								<MxaFooter />
							</div>
					)
					break;
				
				case "txmain":
					surl = "/api/v1/TxMainStru?vuserid=jon&authtoken="+ this.props.authToken;
					durl = "/api/v1/TxMainData?vuserid=jon&authtoken="+ this.props.authToken;
					
					return (
							<div>
								<MxaHeader vUserID={this.props.vUserID} vCompanyID={this.props.vCompanyID} authToken={this.props.authToken} gotLanguageID={this.gotLanguageID} toggleSideBar={this.toggleSideBar} />
								<div className="row">
									<MxaSideBar btnClicked={this.btnClicked} menuUrl={sideBarUrl} />
									<div className ="col-md-8 gridForm">
										<MxaGridForm  struUrl={surl} dataUrl={durl}  steps={[{step:1,stepText:"Step One"}]} ddVisible={false} ddFillUrl1="none" callMaintain={this.callMaintain} />
									</div>
								</div>
								<MxaFooter />
							</div>
					)
					break;
					
			};
			
			
		}
	}
	
export default MxaApp;