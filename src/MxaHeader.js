import React, { Component, PropTypes } from 'react';
import MxaLanguage from './MxaLanguage';

	class MxaHeader extends React.Component {
		constructor(props) {
			super(props);
			
			this.state = {
				language: '',
				mainInfo: null
			}

		}
		
		componentDidMount() {
			console.log("header did mount");
			this.loadData();
		}
		
		loadData(){
			console.log("header load data");
			let headerUrl = "/api/v1/VMainInfo?vuserid=" + this.props.vUserID + "&CompanyID=" + this.props.vCompanyID + "&authtoken=" + this.props.authToken;
			
			$.ajax({
			  method: "GET",
			  url: headerUrl
			}).done(function( data ) {
				console.log("header gotdata");
				this.setState({
				  mainInfo: data
				});
				this.gotLanguageID(data['LanguageID'], data['MenuID']);
			}.bind(this)).fail(function( msg ) {
				console.log("header load data failed "+msg);
			}.bind(this));
			
		}
		
		getCompanyNameLogo(){
			console.log("header getCompanyNameLogo");
			let companyLogoSrc = "data:image/jpg;base64,"+this.state.mainInfo.b64CompanyLogo;
			let companyName = this.state.mainInfo.CompanyName;
			
			return (<div className="MxaHeaderUser" ><img className="img-responsive img-circle" style={{height:'35px',display:'inline-block'}} src={companyLogoSrc} />&nbsp;{companyName}</div>);
		}
		
		getUserPic(){
			console.log("header getUserPic");
			let userPicSrc = "data:image/jpg;base64,"+this.state.mainInfo.b64UserPicture;
			let userName = this.state.mainInfo.FirstName +' '+this.state.mainInfo.LastName;
			
			return (<div className="img-rounded MxaHeaderUser" >{userName}&nbsp;<img className="img-responsive img-circle" style={{height:'35px',display:'inline-block'}} src={userPicSrc} /></div>);
		}
		
		toggleSideBar(){
			this.props.toggleSideBar();
		}
		
		gotLanguageID(languageID, menuID){
			this.props.gotLanguageID(languageID,menuID);
		}
		
		render() {
			let fillurl = "/api/v1/Language?vuserid="+this.props.vUserID+"&authtoken=" + this.props.authToken;
			let languageID = '';
			let userNamePic = '';
			let companyNameLogo = '';
			console.log("header render");

			if(this.state.mainInfo != null){
				console.log('header mainInfo not null');
				languageID = this.state.mainInfo['LanguageID'];
				userNamePic = this.getUserPic();
				companyNameLogo = this.getCompanyNameLogo();
				
				
			}
			
			return (
					<div className="row MxaHeaderRow">
						<div className="col-md-1 text-center MxaHeaderToggle img-rounded" onClick={this.toggleSideBar.bind(this)} ><img className="img-responsive" src="/Content/images/sidebar-toggle-dark.png" /></div>
						<div className="col-md-1 text-center"></div>
						<div className="col-md-3 text-center">{companyNameLogo}</div>
						<div className="col-md-4 text-center"></div>
						<div className="col-md-2 text-center">{userNamePic}</div>
						<div className="col-md-1 text-center">
							<MxaLanguage vUserID={this.props.vUserID} authToken={this.props.authToken} languageID= {languageID} fillUrl={fillurl} />
						</div>
						
					</div>
			)
		}
	}	

	
export default MxaHeader;