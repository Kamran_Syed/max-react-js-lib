import React, { Component, PropTypes } from 'react';


	class MxaMessage extends React.Component {
		constructor(props) {
			super(props);
			this.state = {visible:true};
		}
		
		componentDidMount() {
			this.startTimer();
		}
		
		componentWillUpdate(nextProps, nextState){
			if(nextProps.type != this.props.type || nextProps.message != this.props.message){
				this.state.visible = true;
				this.startTimer();
			}
		}
		
		startTimer(){
			let self = this;
			
			setTimeout(function() {
				self.hide();
			}, this.props.interval);
		}
		
		hide(){
			this.setState({
			  visible: false
			});
		}
		
		render() {
			let styleCss = "MxaMessageSuccess";
			
			if(this.props.message == "none"){
				return false;
			}
			
			if(this.state.visible == true){
				if(this.props.type == "error"){
					styleCss = "MxaMessageError";
				}else if(this.props.type == "warning"){
					styleCss = "MxaMessageWarning";
				}else if(this.props.type == "success"){
					styleCss = "MxaMessageSuccess";
				}
			}else{
				if(this.props.type == "error"){
					styleCss = "MxaMessageError fade";
				}else if(this.props.type == "warning"){
					styleCss = "MxaMessageWarning fade";
				}else if(this.props.type == "success"){
					styleCss = "MxaMessageSuccess fade";
				}
			}
			
			return (
						<div className={styleCss}>{this.props.message}</div>
					)
		}
	}
	
export default MxaMessage;